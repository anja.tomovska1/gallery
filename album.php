<html>
<head>
    <?php require_once 'partials/header.php'; ?>
    <style>
        body {
            background-color: lightgrey;
        }
    </style>
</head>
<body>
<?php require_once 'partials/navbar.php'; ?>

<div class="main-content">
    <div class="image-gallery">
        <div class="main-image">
            <img src="uploads/animal1.jpeg"/>
        </div>

        <div class="image-slider">
            <i class="fas fa-chevron-left" id="slideLeft"></i>
            <div class="image-list-container">
                <div class="image-list">
                    <img src="uploads/animal1.jpeg" class="selected"/>
                    <img src="uploads/animal2.jpeg"/>
                    <img src="uploads/animal3.jpg" />
                    <img src="uploads/animal4.jpg"/>
                    <img src="uploads/animal5.jpg"/>
                    <img src="uploads/animal6.jpg"/>
                    <img src="uploads/animal7.jpg"/>
                </div>
            </div>
            <i class="fas fa-chevron-right" id="slideRight"></i>
        </div>


    </div>
</div>

<script src="assets/js/album.js"></script>
</body>
</html>