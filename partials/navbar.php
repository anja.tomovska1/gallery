<nav class="navbar navbar-expand-lg navbar-light bg-light justify-content-between">
        <a class="navbar-brand" href="index">
            <img src="assets/images/logo-albums.png" width="30" height="30" class="d-inline-block align-top" alt="" loading="lazy">
            Albums Gallery
        </a>
    <div class="collapse navbar-collapse" id="navbarNavAltMarkup">
        <div class="navbar-nav">
            <a class="nav-item nav-link active" href="create">Create <span class="sr-only">(current)</span></a>
            <a class="nav-item nav-link" href="upload">Upload</a>
        </div>
    </div>
</nav>