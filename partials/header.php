<link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.5.0/css/bootstrap.min.css" integrity="sha384-9aIt2nRpC12Uk9gS9baDl411NQApFmC26EwAOH8WgZl5MYYxFfc+NcPb1dKGj7Sk" crossorigin="anonymous">
<link rel="stylesheet" href="assets/styles/index.css">
<link rel="stylesheet" href="assets/styles/upload.css">
<link rel="stylesheet" href="assets/styles/album.css">
<script src="https://kit.fontawesome.com/755054694e.js" crossorigin="anonymous"></script>
