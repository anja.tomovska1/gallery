<?php
?>
<html>
<head>
    <?php require_once 'partials/header.php'; ?>
</head>
<body>
<?php require_once 'partials/navbar.php'; ?>

<div class="main-content">
<form method="post" enctype="multipart/form-data">
    <div class="form-group">
        <label for="inputTitle">Title</label>
        <input type="text" class="form-control" id="inputTitle" name="title">
    </div>
    <div class="form-group">
        <label for="inputDesc">Description</label>
        <textarea class="form-control" id="inputDesc" rows="5" name="description"></textarea>
    </div>
    <div class="form-group">
        <label for="cover">Album Cover</label>
        <input type="file" class="form-control-file" name="cover" id="cover">
    </div>
    <button type="submit" class="btn btn-primary">Submit</button>
</form>
</div>
</body>
</html>
