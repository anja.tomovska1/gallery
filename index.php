<html>
<head>
    <?php require_once 'partials/header.php'; ?>
</head>
<body>
   <?php require_once 'partials/navbar.php'; ?>
<div class="album-modal">
    <div class="album-modal-body">
        <i class="fas fa-times" id="closeModal"></i>
        <div class="title">Some title</div>
        <div class="images">Images 12</div>
        <div class="created">22.01.2020</div>
    </div>

</div>
<div class="main-content">
    <!--  Albums list  -->

    <div class="albums-container">
        <!--    Single album container-->

        <div class="album">
            <div class="album-title">
                <a href="album.php?id=10">Portraits</a>
            </div>
            <div class="album-body">
                <img src="uploads/portrait.jpeg">

            </div>
            <span class="image-count" style="display:none;">12</span>

            <div class="details">
                <i class="fas fa-camera-retro"></i>
                <i class="fas fa-info-circle album-info"></i>
            </div>

        </div>
        <div class="album">
            <div class="album-title">
                <a href="album.php?id=10">Animals</a>
            </div>
            <div class="album-body">
                <img src="uploads/animals2.jpg">

            </div>
            <span class="image-count" style="display:none;">12</span>

            <div class="details">
                <i class="fas fa-camera-retro"></i>
                <i class="fas fa-info-circle album-info"></i>
            </div>

        </div>
        <div class="album">
            <div class="album-title">
                <a href="album.php?id=10">Food</a>
            </div>
            <div class="album-body">
                <img src="uploads/food.jpg">

            </div>
            <span class="image-count" style="display:none;">12</span>

            <div class="details">
                <i class="fas fa-camera-retro"></i>
                <i class="fas fa-info-circle album-info"></i>
            </div>

        </div>
        <div class="album">
            <div class="album-title">
                <a href="album.php?id=10">Nature</a>
            </div>
            <div class="album-body">
                <img src="uploads/nature.jpeg">

            </div>
            <span class="image-count" style="display:none;">12</span>

            <div class="details">
                <i class="fas fa-camera-retro"></i>
                <i class="fas fa-info-circle album-info"></i>
            </div>

        </div>
    </div>

</div>
</div>
<script src="assets/js/index.js"></script>
</body>
</html>
