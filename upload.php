<html>
<head>
    <?php require_once 'partials/header.php'; ?>
</head>
<body>


<?php require_once 'partials/navbar.php'; ?>

<div class="main-content">
    <form method="post"
          enctype="multipart/form-data"
          id="album-images-form"
          name="album-images-form">
        <div class="form-group">
            <div class="input-group">
                <select class="custom-select" id="select-album" name="albumId">
                    <option value="">Select Album</option>
                        <option value="1">Album 1</option>
                        <option value="2">Album 2</option>
                        <option value="3">Album 3</option>
                </select>
            </div>
        </div>
        <input type="file" accept="image/*" multiple style="visibility: hidden" name="images[]">

        <div class="preview-images">

        </div>
        <div class="form-group">
            <button type="button" class="btn  btn-secondary btn-sm" id="upload-btn">
                <i class="fas fa-camera-retro"></i>Upload Images
            </button>
        </div>

        <button type="submit" class="btn btn-primary">Submit</button>
    </form>
</div>

</body>
<script src="assets/js/upload.js"></script>
</html>
